package hig.yog;

import android.test.AndroidTestCase;
import android.util.Log;

import java.util.UUID;

import hig.yog.containers.StoryItem;
import hig.yog.containers.User;
import hig.yog.db.RemoteDBHelper;

/**
 * Created by mityakoval on 5/12/15.
 */
public class RemoteDBTest extends AndroidTestCase {

    User user;
    StoryItem item;
    RemoteDBHelper dbHelper;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        user = new User();
        dbHelper = new RemoteDBHelper();
        item = new StoryItem();
        item.setId(1);
        item.setName("test");
        item.setDescription("test");
        user.setId(1);
        user.setFirstName("John");
        user.setSecondName("Test");
        user.setEmail(UUID.randomUUID().toString() + "@example.com");
        user.setPoints(123);
    }

    public void testPOSTUser() {
        boolean result = dbHelper.POST("user.json", user.convertToJSONString());
        assertTrue(result);
        Log.d("POST", "\'POSTUser\' method test passed");
    }

    public void testPUTUser() {
        user.setFirstName("Jim");
        boolean result = dbHelper.PUT(user.getURI(), user.convertToJSONString());
        assertTrue(result);
        Log.d("PUT", "PUT method test passed");
    }

//    public void testDELETE(){
//        boolean result = dbHelper.DELETE("user/2.json");
//        assertTrue(result);
//        Log.d("DELETE", "DELETE method test passed");
//    }
}

