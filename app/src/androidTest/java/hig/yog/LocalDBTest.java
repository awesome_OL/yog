package hig.yog;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;
import android.util.Log;

import hig.yog.containers.Quest;
import hig.yog.containers.User;
import hig.yog.db.LocalDbHelper;

/**
 * Created by mityakoval on 5/7/15.
 */
public class LocalDBTest extends AndroidTestCase {

    private static int id = 0;
    private static String firstName = "John";
    private static String secondName = "Test";
    private static String email = "qwerty@example.com";
    private static int points = 123;
    private static String questName = "test";
    private static String questDescription = "test";
    private static int questPoints = 123;

    public void testCreateDB(){
        LocalDbHelper dbHelper = new LocalDbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        assertTrue(db.isOpen());
        db.close();
        Log.d("Database testing", "testCreateDb passed");
    }

    public void testInsertUserData(){
        LocalDbHelper dbHelper = new LocalDbHelper(mContext);

        long id = dbHelper.addUser(firstName, secondName, email, points);

        assertTrue(id != -1);

        Log.d("Database testing", "testInsertUserData passed");
    }

    public void testRetrieveUserData(){
        LocalDbHelper dbHelper = new LocalDbHelper(mContext);

        User user = dbHelper.getUser();

        assertEquals(id, user.getId());
        assertEquals(firstName, user.getFirstName());
        assertEquals(secondName, user.getSecondName());
        assertEquals(email, user.getEmail());
        assertEquals(points, user.getPoints());

        Log.d("Database testing", "testRetrieveUserData passed");
    }

    public void testInsertQuestData(){
        LocalDbHelper dbHelper = new LocalDbHelper(mContext);

        long id = dbHelper.addQuest(questName, questDescription, null, null, questPoints);

        assertTrue(id != -1);

        Log.d("Database testing", "testInsertQuestData passed");
    }

    public void testRetrieveQuestData(){
        LocalDbHelper dbHelper = new LocalDbHelper(mContext);

        Quest quest = dbHelper.getQuest();

        assertEquals(questName, quest.getName());
        assertEquals(questDescription, quest.getDescription());
        assertEquals(null, quest.getLocationLatitude());
        assertEquals(null, quest.getLocationLongitude());
        assertEquals(questPoints, quest.getPoints());
    }

    public void testDropDB(){
        assertTrue(mContext.deleteDatabase(LocalDbHelper.DATABASE_NAME));

        Log.d("Database testing", "testDropDB passed");
    }
}
