package hig.yog.slidingtab;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import hig.yog.R;

/**
 * Created by mityakoval on 4/10/15.
 */
public class Tab1 extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profile_tab_profile, container, false);
        ImageView profileIcon = (ImageView) view.findViewById(R.id.profileImageView);
        profileIcon.setImageResource(R.drawable.def_profile_icon);
        return view;
    }
}
