package hig.yog;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

/**
 * <h1>Story activity</h1>
 * Activity for the Story page
 *
 * @author Mitya Koval
 * @version 1.0
 */
public class StoryActivity extends BaseActivity {

    public final static String EXTRA_MESSAGE = "hig.yog.MESSAGE";

    TextView storyItemName;
    TextView storyItemDescription;
    Button questButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story);

        storyItemName = (TextView) findViewById(R.id.storyNameTextView);
        storyItemDescription = (TextView) findViewById(R.id.storyDescTextView);
        questButton = (Button) findViewById(R.id.questButton);
    }
}
