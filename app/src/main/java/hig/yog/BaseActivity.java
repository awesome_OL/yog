package hig.yog;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import hig.yog.containers.Quest;
import hig.yog.containers.StoryItem;
import hig.yog.containers.User;
import hig.yog.db.LocalDbHelper;

/**
 * <h1>Base activity</h1>
 * Provides activities that extends it with a single action bar for the whole application
 *
 * @author Mitya Koval
 * @version 1.0
 */
public class BaseActivity extends ActionBarActivity {
    ActionBar actionBar;

    private static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        actionBar = getActionBar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        User user = new User(0, "John", "Test", "example@example.com");
        StoryItem storyItem = new StoryItem(0, "Story 1", "This is the first story item.");
        Quest quest = new Quest(0, "Quest 1", "This is the first quest", 10);
        Object [] objects = { user, storyItem, quest};
        new InsertIntoDBTask().execute(objects);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id){
            case R.id.menu_profile: {
                Intent intent = new Intent(this, ProfileActivity.class);
                startActivity(intent);
                break;
            }

            case R.id.menu_map: {
                Intent intent = new Intent(this, MapsActivity.class);
                startActivity(intent);
                break;
            }

            case R.id.menu_story: {
                Intent intent = new Intent(this, StoryActivity.class);
                startActivity(intent);
                break;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private class InsertIntoDBTask extends AsyncTask<Object, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Object... objects) {
            //boolean success = true;
            LocalDbHelper dbHelper = new LocalDbHelper(getBaseContext());

            if (dbHelper.isUsersTableEmpty()) {
                User user = (User) objects[0];
                dbHelper.addUser(user.getFirstName(), user.getSecondName(), user.getEmail(), user.getPoints());
            }
            if (dbHelper.isStoriesTableEmpty()) {
                StoryItem story = (StoryItem) objects[1];
                dbHelper.addStoryItem(story.getName(), story.getDescription());
            }
            if (dbHelper.isQuestsTableEmpty()) {
                Quest quest = (Quest) objects[2];
                dbHelper.addQuest(quest.getName(), quest.getDescription(),
                        quest.getLocationLatitude(), quest.getLocationLongitude(), quest.getPoints());
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);
        }
    }
}
