package hig.yog.containers;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by mityakoval on 4/7/15.
 */
public class Team {

    private int id;
    private String name;
    private int points;
    private ArrayList<User> members;

    public Team() {
    }

    public Team(int id, String name) {
        this.id = id;
        this.name = name;
        members = new ArrayList<>();
    }

    public Team(JSONObject jsonObject){
        try {
            this.id = jsonObject.getInt("id");
            this.name = jsonObject.getString("name");
            this.points = jsonObject.getInt("points");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public ArrayList<User> getMembers() {
        return members;
    }

    public void setMembers(ArrayList<User> members) {
        this.members = members;
    }

    public String convertToJSONString(){
        JSONObject teamObject = new JSONObject();
        JSONObject jsonObject = new JSONObject();

        try {

            teamObject.put("name", this.name);
            teamObject.put("points", this.points);

            jsonObject.put("team", teamObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    public String getIdURI(){
        return "team/" + this.id + ".json";
    }
}
