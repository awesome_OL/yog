package hig.yog.containers;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by mityakoval on 4/7/15.
 */
public class User {

    private int id;
    private String firstName;
    private String secondName;
    private String email;
    private int points;

    public User() {
    }

    public User(int id, String firstName, String secondName, String email) {
        this.id = id;
        this.firstName = firstName;
        this.secondName = secondName;
        this.email = email;
    }

    public User(JSONObject jsonObject) {
        try {
            this.id = jsonObject.getInt("id");
            this.firstName = jsonObject.getString("first_name");
            this.secondName = jsonObject.getString("second_name");
            this.email = jsonObject.getString("email");
            this.points = jsonObject.getInt("points");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String convertToJSONString(){
        JSONObject userObject = new JSONObject();
        JSONObject jsonObject = new JSONObject();

        try {

            userObject.put("first_name", firstName);
            userObject.put("second_name", secondName);
            userObject.put("email", email);
            userObject.put("points", points);

            jsonObject.put("user", userObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    public String getURI(){
        return "user/" + this.id + ".json";
    }


}
