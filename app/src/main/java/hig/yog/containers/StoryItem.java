package hig.yog.containers;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by mityakoval on 4/7/15.
 */
public class StoryItem {

    private int id;
    private String name;
    private String description;

    public StoryItem() {
    }

    public StoryItem(int id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public StoryItem(JSONObject jsonObject){
        try {
            this.id = jsonObject.getInt("id");
            this.name = jsonObject.getString("name");
            this.description = jsonObject.getString("description");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String convertToJSONString(){
        JSONObject storyObject = new JSONObject();
        JSONObject jsonObject = new JSONObject();

        try {

            storyObject.put("name", this.name);
            storyObject.put("description", this.description);

            jsonObject.put("story_item", storyObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    public String getIdURI(){
        return "story/" + this.id + ".json";
    }
}
