package hig.yog.containers;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by mityakoval on 4/7/15.
 */
public class Quest {

    private int id;
    private String name;
    private String description;
    private String locationLatitude;
    private String locationLongitude;
    private int points;

    public Quest() {
    }

    public Quest(int id, String name, String description, int points) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.points = points;
    }

    public Quest(JSONObject jsonObject) {
        try {
            this.id = jsonObject.getInt("id");
            this.name = jsonObject.getString("name");
            this.description = jsonObject.getString("description");
            if (jsonObject.getString("location_latitude") != null) {
                this.locationLatitude = jsonObject.getString("location_latitude");
                this.locationLongitude = jsonObject.getString("location_longitude");
            }
            this.points = jsonObject.getInt("points");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocationLatitude() {
        return locationLatitude;
    }

    public void setLocationLatitude(String locationLatitude) {
        this.locationLatitude = locationLatitude;
    }

    public String getLocationLongitude() {
        return locationLongitude;
    }

    public void setLocationLongitude(String locationLongitude) {
        this.locationLongitude = locationLongitude;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String convertToJSONString(){
        JSONObject questObject = new JSONObject();
        JSONObject jsonObject = new JSONObject();

        try {

            questObject.put("name", this.name);
            questObject.put("description", this.description);

            jsonObject.put("quest", questObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    public String getIdURI(){
        return "quest/" + this.id + ".json";
    }
}
