package hig.yog;

import android.os.Bundle;
import android.support.v4.view.ViewPager;

import hig.yog.slidingtab.SlidingTabLayout;
import hig.yog.slidingtab.ViewPagerAdapter;

/**
 * <h1>Profile activity</h1>
 * Activity for the Profile page
 *
 * @author Mitya Koval
 * @version 1.0
 */
public class ProfileActivity extends BaseActivity {

    ViewPager pager;
    ViewPagerAdapter adapter;
    SlidingTabLayout tabs;
    CharSequence titles[]={"Profile", "Team"};
    int numberOfTabs = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        adapter = new ViewPagerAdapter(getSupportFragmentManager(), titles, numberOfTabs);

        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);

        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true);

        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.tabsScrollColor);
            }
        });

        tabs.setViewPager(pager);
    }


}
