package hig.yog.db;

import android.provider.BaseColumns;

import hig.yog.containers.User;

/**
 * Created by mityakoval on 5/5/15.
 */
public final class DataContract {

//    public static final String SQL_CREATE_TABLES = User.SQL_CREATE + Team.SQL_CREATE +
//                                                   Teammates.SQL_CREATE + StoryItem.SQL_CREATE +
//                                                   Quest.SQL_CREATE;
//    public static final String SQL_DROP_TABLES = User.SQL_DROP + Team.SQL_DROP +
//                                                 Teammates.SQL_DROP + StoryItem.SQL_DROP +
//                                                 Quest.SQL_DROP;

    public static final String TEXT_TYPE = " TEXT ";
    public static final String INT_TYPE = " INTEGER ";
    public static final String COMMA_SEP = ", ";

    public DataContract(){};

    public static abstract class User implements BaseColumns{
        public static final String TABLE_NAME = "users";
        public static final String COLUMN_NAME_USER_FIRSTNAME = "first_name";
        public static final String COLUMN_NAME_USER_SECONDNAME = "second_name";
        public static final String COLUMN_NAME_USER_EMAIL = "email";
        public static final String COLUMN_NAME_USER_POINTS = "points";

        public static final String SQL_CREATE = "CREATE TABLE " + TABLE_NAME + "(" +
                _ID + " INTEGER PRIMARY KEY NOT NULL," +
                COLUMN_NAME_USER_FIRSTNAME + TEXT_TYPE + COMMA_SEP +
                COLUMN_NAME_USER_SECONDNAME + TEXT_TYPE + COMMA_SEP +
                COLUMN_NAME_USER_EMAIL + TEXT_TYPE + COMMA_SEP +
                COLUMN_NAME_USER_POINTS + INT_TYPE + "); ";

        public static final String SQL_DROP = "DROP TABLE IF EXISTS" + TABLE_NAME;
    }

    public static abstract class Team implements BaseColumns{
        public static final String TABLE_NAME = "teams";
        public static final String COLUMN_NAME_TEAM_NAME = "name";
        public static final String COLUMN_NAME_POINTS = "points";

        public static final String SQL_CREATE = "CREATE TABLE " + TABLE_NAME + "(" +
                _ID + " INTEGER PRIMARY KEY NOT NULL, " +
                COLUMN_NAME_TEAM_NAME + TEXT_TYPE + COMMA_SEP +
                COLUMN_NAME_POINTS + INT_TYPE + "); ";

        public static final String SQL_DROP = "DROP TABLE IF EXISTS" + TABLE_NAME + "; ";
    }

    public static abstract class Teammates implements BaseColumns{
        public static final String TABLE_NAME = "teammates";
        public static final String COLUMN_NAME_TEAMMATE_FIRSTNAME = "first_name";
        public static final String COLUMN_NAME_TEAMMATE_SECONDNAME = "second_name";
        public static final String COLUMN_NAME_TEAMMATE_POINTS = "points";

        public static final String SQL_CREATE = "CREATE TABLE " + TABLE_NAME + "(" +
                _ID + " INTEGER PRIMARY KEY NOT NULL, " +
                COLUMN_NAME_TEAMMATE_FIRSTNAME + TEXT_TYPE + COMMA_SEP +
                COLUMN_NAME_TEAMMATE_SECONDNAME + TEXT_TYPE + COMMA_SEP +
                COLUMN_NAME_TEAMMATE_POINTS + INT_TYPE + "); ";

        public static final String SQL_DROP = "DROP TABLE IF EXISTS" + TABLE_NAME + "; ";
    }

    public static abstract class StoryItem implements BaseColumns{
        public static final String TABLE_NAME = "story_item";
        public static final String COLUMN_NAME_STORY_ITEM_NAME = "name";
        public static final String COLUMN_NAME_STORY_ITEM_DESCRIPTION = "description";

        public static final String SQL_CREATE = "CREATE TABLE " + TABLE_NAME + "(" +
                _ID + " INTEGER PRIMARY KEY NOT NULL, " +
                COLUMN_NAME_STORY_ITEM_NAME + TEXT_TYPE + COMMA_SEP +
                COLUMN_NAME_STORY_ITEM_DESCRIPTION + TEXT_TYPE + "); ";

        public static final String SQL_DROP = "DROP TABLE IF EXISTS" + TABLE_NAME + "; ";
    }

    public static abstract class Quest implements BaseColumns{
        public static final String TABLE_NAME = "quests";
        public static final String COLUMN_NAME_QUEST_NAME = "name";
        public static final String COLUMN_NAME_QUEST_DESCRIPTION = "description";
        public static final String COLUMN_NAME_QUEST_LOCATION_LATITUDE = "location_latitude";
        public static final String COLUMN_NAME_QUEST_LOCATION_LONGITUDE = "location_longitude";
        public static final String COLUMN_NAME_QUEST_POINTS = "points";

        public static final String SQL_CREATE = "CREATE TABLE " + TABLE_NAME + "(" +
                _ID + " INTEGER PRIMARY KEY NOT NULL, " +
                COLUMN_NAME_QUEST_NAME + TEXT_TYPE + COMMA_SEP +
                COLUMN_NAME_QUEST_DESCRIPTION + TEXT_TYPE + COMMA_SEP +
                COLUMN_NAME_QUEST_LOCATION_LATITUDE + TEXT_TYPE + COMMA_SEP +
                COLUMN_NAME_QUEST_LOCATION_LONGITUDE + INT_TYPE + COMMA_SEP +
                COLUMN_NAME_QUEST_POINTS + INT_TYPE + "); ";

        public static final String SQL_DROP = "DROP TABLE IF EXISTS" + TABLE_NAME + "; ";
    }
}
