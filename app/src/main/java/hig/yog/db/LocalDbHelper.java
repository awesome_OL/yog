package hig.yog.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import hig.yog.containers.*;

/**
 * <h1>Local Database Helper</h1>
 * Provides functionality for local SQLite database
 * Creates, upgrades and drops DB. Writes and retrieves data to/from the DB.
 *
 * @author Mitya Koval
 * @version 1.0
 */
public class LocalDbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String  DATABASE_NAME = "YOG.db";
    SQLiteDatabase writableDb = getWritableDatabase();
    SQLiteDatabase readableDb = getReadableDatabase();

    public LocalDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DataContract.User.SQL_CREATE);
        db.execSQL(DataContract.Team.SQL_CREATE);
        db.execSQL(DataContract.StoryItem.SQL_CREATE);
        db.execSQL(DataContract.Quest.SQL_CREATE);
        db.execSQL(DataContract.Teammates.SQL_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DataContract.User.SQL_DROP);
        db.execSQL(DataContract.Team.SQL_DROP);
        db.execSQL(DataContract.StoryItem.SQL_DROP);
        db.execSQL(DataContract.Quest.SQL_DROP);
        db.execSQL(DataContract.Teammates.SQL_DROP);
        onCreate(db);
    }

    /**
     * <h1>Add User</h1>
     * Writes user data into DB
     *
     * @param firstName - User's first name
     * @param secondName - User's second name
     * @param email - User's email address
     * @param points - User's points
     * @return - returns '-1' if data failed to write
     */
    public long addUser(String firstName, String secondName, String email, int points){
        ContentValues values = new ContentValues();

        values.put(DataContract.User.COLUMN_NAME_USER_FIRSTNAME, firstName);
        values.put(DataContract.User.COLUMN_NAME_USER_SECONDNAME, secondName);
        values.put(DataContract.User.COLUMN_NAME_USER_EMAIL, email);
        values.put(DataContract.User.COLUMN_NAME_USER_POINTS, points);


        return writableDb.insert(DataContract.User.TABLE_NAME, null, values);
    }


    /**
     * <h1>Get User</h1>
     * Retrieves User data from the DB and puts it in the User class object.
     *
     * @return - returns User class object
     */
    public User getUser(){
        User user;

        String[] projection = { DataContract.User._ID,
                                DataContract.User.COLUMN_NAME_USER_FIRSTNAME,
                                DataContract.User.COLUMN_NAME_USER_SECONDNAME,
                                DataContract.User.COLUMN_NAME_USER_EMAIL,
                                DataContract.User.COLUMN_NAME_USER_POINTS };

        Cursor c = readableDb.query(DataContract.User.TABLE_NAME, projection, null, null, null, null, null);
        c.moveToFirst();

        user = new User();

        user.setId(c.getColumnIndexOrThrow(DataContract.User._ID));
        user.setFirstName(c.getString(c.getColumnIndexOrThrow(DataContract.User.COLUMN_NAME_USER_FIRSTNAME)));
        user.setSecondName(c.getString(c.getColumnIndexOrThrow(DataContract.User.COLUMN_NAME_USER_SECONDNAME)));
        user.setEmail(c.getString(c.getColumnIndexOrThrow(DataContract.User.COLUMN_NAME_USER_EMAIL)));
        user.setPoints(c.getInt(c.getColumnIndexOrThrow(DataContract.User.COLUMN_NAME_USER_POINTS)));

        c.close();
        return user;
    }

    /**
     * <h1>Add story item</h1>
     * Writes Story item data into DB
     *
     * @param name - Story item name
     * @param description - Story item description
     * @return - '-1' if writing failed
     */
    public long addStoryItem(String name, String description){
        ContentValues values = new ContentValues();

        values.put(DataContract.StoryItem.COLUMN_NAME_STORY_ITEM_NAME, name);
        values.put(DataContract.StoryItem.COLUMN_NAME_STORY_ITEM_DESCRIPTION, description);

        return writableDb.insert(DataContract.StoryItem.TABLE_NAME, null, values);
    }

    /**
     * <h1>Get story item</h1>
     * Retrieves story item data from DB an puts it in the StoryItem class object
     *
     * @return - StoryItem class object
     */
    public StoryItem getStoryItem(){
        StoryItem item;

        String[] projection = { DataContract.StoryItem._ID,
                                DataContract.StoryItem.COLUMN_NAME_STORY_ITEM_NAME,
                                DataContract.StoryItem.COLUMN_NAME_STORY_ITEM_DESCRIPTION };

        item = new StoryItem();

        Cursor c = readableDb.query(DataContract.StoryItem.TABLE_NAME, projection, null, null, null, null, null);
        c.moveToFirst();

        item.setId(c.getInt(c.getColumnIndexOrThrow(DataContract.StoryItem._ID)));
        item.setName(c.getString(c.getColumnIndexOrThrow(DataContract.StoryItem.COLUMN_NAME_STORY_ITEM_NAME)));
        item.setDescription(c.getString(c.getColumnIndexOrThrow(DataContract.StoryItem.COLUMN_NAME_STORY_ITEM_DESCRIPTION)));

        c.close();

        return item;
    }

    /**
     * <h1>Add quest</h1>
     * Writes quest data into DB
     *
     * @param name - quest name
     * @param description - quest description
     * @param latitude - latitude of quest's location
     * @param longitude - longitude quest's location
     * @param points - quest points
     * @return - '-1' if writing failed
     */
    public long addQuest(String name, String description, String latitude, String longitude, int points){
        ContentValues values = new ContentValues();

        values.put(DataContract.Quest.COLUMN_NAME_QUEST_NAME, name);
        values.put(DataContract.Quest.COLUMN_NAME_QUEST_DESCRIPTION, description);
        if (latitude != null) {
            values.put(DataContract.Quest.COLUMN_NAME_QUEST_LOCATION_LATITUDE, latitude);
            values.put(DataContract.Quest.COLUMN_NAME_QUEST_LOCATION_LONGITUDE, longitude);
        }
        values.put(DataContract.Quest.COLUMN_NAME_QUEST_POINTS, points);

        return  writableDb.insert(DataContract.Quest.TABLE_NAME, DataContract.Quest.COLUMN_NAME_QUEST_LOCATION_LATITUDE, values);
    }

    /**
     * <h1>Get quest</h1>
     * Retrieves quest data from DB and puts it in Quest class object
     *
     * @return - Quest class object
     */
    public Quest getQuest(){
        Quest quest;

        String[] projection = { DataContract.Quest._ID,
                                DataContract.Quest.COLUMN_NAME_QUEST_NAME,
                                DataContract.Quest.COLUMN_NAME_QUEST_DESCRIPTION,
                                DataContract.Quest.COLUMN_NAME_QUEST_LOCATION_LATITUDE,
                                DataContract.Quest.COLUMN_NAME_QUEST_LOCATION_LONGITUDE,
                                DataContract.Quest.COLUMN_NAME_QUEST_POINTS };

        Cursor c = readableDb.query(DataContract.Quest.TABLE_NAME, projection, null, null, null, null, null);
        c.moveToFirst();

        quest = new Quest();

        quest.setId(c.getInt(c.getColumnIndexOrThrow(DataContract.Quest._ID)));
        quest.setName(c.getString(c.getColumnIndexOrThrow(DataContract.Quest.COLUMN_NAME_QUEST_NAME)));
        quest.setDescription(c.getString(c.getColumnIndexOrThrow(DataContract.Quest.COLUMN_NAME_QUEST_DESCRIPTION)));

        if( c.getString(c.getColumnIndexOrThrow(DataContract.Quest.COLUMN_NAME_QUEST_LOCATION_LATITUDE)) != null) {
            quest.setLocationLatitude(c.getString(c.getColumnIndexOrThrow(DataContract.Quest.COLUMN_NAME_QUEST_LOCATION_LATITUDE)));
            quest.setLocationLongitude(c.getString(c.getColumnIndexOrThrow(DataContract.Quest.COLUMN_NAME_QUEST_LOCATION_LONGITUDE)));
        }

        quest.setPoints(c.getInt(c.getColumnIndexOrThrow(DataContract.Quest.COLUMN_NAME_QUEST_POINTS)));

        return quest;
    }

    public boolean isUsersTableEmpty(){
        boolean empty = true;
        String countQuery = "SELECT count(*) FROM " + DataContract.User.TABLE_NAME + ";";
        Cursor c = writableDb.rawQuery(countQuery, null);
        c.moveToFirst();
        int count = c.getInt(0);
        if (count > 0)
           empty = false;
        return empty;
    }

    public boolean isStoriesTableEmpty(){
        boolean empty = true;
        String countQuery = "SELECT count(*) FROM " + DataContract.StoryItem.TABLE_NAME + ";";
        Cursor c = writableDb.rawQuery(countQuery, null);
        c.moveToFirst();
        int count = c.getInt(0);
        if (count > 0)
            empty = false;
        return empty;
    }

    public boolean isQuestsTableEmpty(){
        boolean empty = true;
        String countQuery = "SELECT count(*) FROM " + DataContract.Quest.TABLE_NAME + ";";
        Cursor c = writableDb.rawQuery(countQuery, null);
        c.moveToFirst();
        int count = c.getInt(0);
        if (count > 0)
            empty = false;
        return empty;
    }
}
