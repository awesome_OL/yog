package hig.yog.db;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import hig.yog.containers.Quest;
import hig.yog.containers.StoryItem;
import hig.yog.containers.Team;
import hig.yog.containers.User;

/**
 * <h1>Remote Database Helper</h1>
 * Provides functionality for GET, POST, PUT, DELETE requests
 *
 * @author Mitya Koval
 * @version version 1.0
 */
public class RemoteDBHelper {

    public static final String URI = "http://yog-server.herokuapp.com/";

    public RemoteDBHelper(){}

    /**
     * <h1>GET</h1>
     * Sends GET request to a URI and returns expected Object specified in parameters
     *
     * @param URI - URI to send a request
     * @param expectedObject - object class that must be returned
     * @return - object of the requested class
     */
    public Object GET(String URI, Class expectedObject){
        Object result = null;

        JSONObject jsonObject = null;

        InputStream is;

        HttpClient httpClient = new DefaultHttpClient();

        HttpGet httpGet = new HttpGet(this.URI + URI);

        try {
            HttpResponse response = httpClient.execute(httpGet);

            is = response.getEntity().getContent();
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();

            if (is != null & statusCode == 200) {
                jsonObject = convertInputStreamToJSON(is);

                if (jsonObject != null) {
                    if (expectedObject == User.class) {
                        User user = new User(jsonObject);
                        result = user;
                    }
                    if (expectedObject == Quest.class) {
                        Quest quest = new Quest(jsonObject);
                        result = quest;
                    }
                    if (expectedObject == StoryItem.class) {
                        StoryItem storyItem = new StoryItem(jsonObject);
                        result = storyItem;
                    }
                    if (expectedObject == Team.class) {
                        Team team = new Team(jsonObject);
                        result = team;
                    }
                }
            }

        } catch (IOException | JSONException e) {
            Log.d("GET", e.getLocalizedMessage());
        }

        return result;
    }

    /**
     * <h1>POST</h1>
     * Sends POST request to a URI and returns HTTP response code as 'true' or 'false'
     *
     * @param URI - URI to send a request
     * @param jsonString - stringified JSON object to POST
     * @return - 'true' if HTTP response code equals "200", false - otherwise
     */
    public boolean POST(String URI, String jsonString){
        boolean result = false;

        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(this.URI + URI);

        try{
            StringEntity se = new StringEntity(jsonString);

            post.setEntity(se);

            post.setHeader("Content-Type", "application/json");

            HttpResponse response = client.execute(post);

            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();

            if (statusCode == 201)
                result = true;

        } catch (IOException e) {
            Log.d("POST", e.getLocalizedMessage());
        }


        return result;
    }

    /**
     * <h1>PUT</h1>
     * Sends PUT request to a URI and returns HTTP response code as 'true' or 'false'
     *
     * @param URI - URI to send a request
     * @param jsonString - stringified JSON object to PUT
     * @return - 'true' if HTTP response code equals "200", false - otherwise
     */
    public boolean PUT(String URI, String jsonString){
        boolean result = false;

        HttpClient client = new DefaultHttpClient();
        HttpPut put  = new HttpPut(this.URI + URI);

        try{
            StringEntity se = new StringEntity(jsonString);

            put.setEntity(se);

            put.setHeader("Content-Type", "application/json");

            HttpResponse response = client.execute(put);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();

            if (statusCode == 200)
                result = true;

        } catch (IOException e) {
            Log.d("PUT", e.getLocalizedMessage());
        }

        return  result;
    }

    /**
     * <h1>DELETE</h1>
     * Sends DELETE request to a URI and returns HTTP response code as 'true' or 'false'
     *
     * @param URI - URI to send a request
     * @return - 'true' if HTTP response code equals "200", 'false' - otherwise
     */
    public boolean DELETE(String URI){
        boolean result = false;

        HttpClient client = new DefaultHttpClient();
        HttpDelete delete = new HttpDelete(this.URI +URI);

        try{
            HttpResponse response = client.execute(delete);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();

            if (statusCode == 200)
                result = true;

        } catch (IOException e) {
            Log.d("DELETE", e.getLocalizedMessage());
        }

        return result;
    }

    private JSONObject convertInputStreamToJSON(InputStream is) throws IOException, JSONException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        String line;
        String response = "";
        while((line = reader.readLine()) != null)
            response += line;

        return new JSONObject(response);
    }

}
